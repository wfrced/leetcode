package two_sum

func twoSum(nums []int, target int) []int {
	// Check if there are at least two elements
	// in the array
	numsLen := len(nums)
	if numsLen < 2 {
		return nil
	}

	numMap := make(map[int]int)
	for idx, num := range nums {
		complement := target - num
		mIdx, ok := numMap[complement]
		if ok {
			return []int{mIdx, idx}
		}

		numMap[num] = idx
	}

	return nil
}
