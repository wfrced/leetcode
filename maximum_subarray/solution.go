package maximum_subarray

func maxInt(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func maxSubArray(nums []int) int {
	maxGlobal := nums[0]
	maxCurrent := nums[0]
	for i := 1; i < len(nums); i++ {
		maxCurrent = maxInt(nums[i], maxCurrent+nums[i])
		maxGlobal = maxInt(maxGlobal, maxCurrent)
	}

	return maxGlobal
}
