package best_time_to_buy_and_sell_stock

func maxProfit(prices []int) int {
	profit := 0
	minPrice := 2147483647
	for _, price := range prices {
		if price < minPrice {
			minPrice = price
		} else if profit < price-minPrice {
			profit = price - minPrice
		}
	}

	return profit
}
