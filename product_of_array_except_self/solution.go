package product_of_array_except_self

// Uses prefix and suffix products to calculate the answer.
func productExceptSelf(nums []int) []int {

	lProd := make([]int, len(nums))
	lCurr := 1
	for i := 0; i < len(nums); i++ {
		lProd[i] = lCurr
		lCurr = lCurr * nums[i]
	}

	rProd := make([]int, len(nums))
	rCurr := 1
	for i := len(nums) - 1; i >= 0; i-- {
		rProd[i] = rCurr
		rCurr = rCurr * nums[i]
	}

	products := make([]int, len(nums))
	for i, _ := range nums {
		products[i] = rProd[i] * lProd[i]
	}

	return products
}
