package contains_duplicate_iii

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_containsNearbyAlmostDuplicate(t *testing.T) {
	type args struct {
		nums []int
		k    int
		t    int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "contains duplicate",
			args: args{
				nums: []int{
					1, 2, 3, 1,
				},
				k: 3,
				t: 0,
			},
			want: true,
		},
		{
			name: "contains duplicate",
			args: args{
				nums: []int{
					1, 0, 1, 1,
				},
				k: 1,
				t: 2,
			},
			want: true,
		},
		{
			name: "contains duplicate",
			args: args{
				nums: []int{
					1, 5, 9, 1, 5, 9,
				},
				k: 2,
				t: 3,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, containsNearbyAlmostDuplicate(tt.args.nums, tt.args.k, tt.args.t))
		})
	}
}
