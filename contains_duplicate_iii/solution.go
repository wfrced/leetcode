package contains_duplicate_iii

func bucket(num, t int) int {
	if t == 0 || num == 0 {
		return num
	}

	if num > 0 {
		return num / (t + 1)
	}
	return num/t - 1
}

func containsNearbyAlmostDuplicate(nums []int, k int, t int) bool {
	buckets := make(map[int]int)

	for i, num := range nums {
		if i > k {
			delete(buckets, bucket(nums[i-k-1], t))
		}

		bkt := bucket(nums[i], t)

		if _, ok := buckets[bkt]; ok {
			return true
		}

		if val, ok := buckets[bkt-1]; ok && num-val <= t {
			return true
		}

		if val, ok := buckets[bkt+1]; ok && val-num <= t {
			return true
		}

		buckets[bkt] = num
	}

	return false

}
