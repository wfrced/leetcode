package best_time_to_buy_and_sell_stock_iii

// less boilerplate
// why is there no minmax functions in Go tho
func minInt(x, y int) int {
	if x < y {
		return x
	}
	return y
}
func maxInt(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func maxProfit(prices []int) int {
	profit := 0
	pricesLen := len(prices)

	//var left, right [pricesLen]int
	left := make([]int, pricesLen)
	right := make([]int, pricesLen)

	left[0], right[pricesLen-1] = 0, 0
	min := prices[0]
	max := prices[pricesLen-1]

	for i := 1; i < pricesLen; i++ {
		min = minInt(min, prices[i])
		left[i] = maxInt(left[i-1], prices[i]-min)
	}

	for i := pricesLen - 2; i >= 0; i-- {
		max = maxInt(max, prices[i])
		right[i] = maxInt(right[i+1], max-prices[i])
	}

	for i, _ := range prices {
		profit = maxInt(profit, left[i]+right[i])
	}

	return profit
}
