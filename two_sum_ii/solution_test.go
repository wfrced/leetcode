package two_sum_ii

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_twoSum(t *testing.T) {
	type args struct {
		nums   []int
		target int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "twoSum",
			args: args{
				nums:   []int{2, 7, 11, 15},
				target: 9,
			},
			want: []int{1, 2},
		},
		{
			name: "twoSum",
			args: args{
				nums:   []int{2, 2, 11, 15},
				target: 4,
			},
			want: []int{1, 2},
		},
		{
			name: "twoSum",
			args: args{
				nums:   []int{2, 3, 4},
				target: 6,
			},
			want: []int{1, 3},
		},
		{
			name: "twoSum",
			args: args{
				nums:   []int{-1, 0},
				target: -1,
			},
			want: []int{1, 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, twoSum(tt.args.nums, tt.args.target))
		})
	}
}
