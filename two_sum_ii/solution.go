package two_sum_ii

func twoSum(nums []int, target int) []int {
	// Check if there are at least two elements
	// in the array
	numsLen := len(nums)
	if numsLen < 2 {
		return nil
	}

	front := 0
	rear := numsLen - 1
	for front != rear {
		twoSum := nums[front] + nums[rear]
		if twoSum == target {
			return []int{
				// Guess it's a check so that you don't
				// submit the previous solution
				front + 1,
				rear + 1,
			}
		}

		if twoSum < target {
			front++
		} else {
			rear--
		}
	}

	return nil
}
