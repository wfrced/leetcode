package maximum_product_subarray

func minInt(x, y int) int {
	if x < y {
		return x
	}
	return y
}
func maxInt(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func maxProduct(nums []int) int {
	minCurrent := nums[0]
	maxCurrent := nums[0]
	maxGlobal := nums[0]

	for i := 1; i < len(nums); i++ {
		// A catch: negative number swaps minimum and maximum values
		if nums[i] < 0 {
			minCurrent, maxCurrent = maxCurrent, minCurrent
		}

		minCurrent = minInt(nums[i], minCurrent*nums[i])
		maxCurrent = maxInt(nums[i], maxCurrent*nums[i])
		maxGlobal = maxInt(maxGlobal, maxCurrent)
	}

	return maxGlobal
}
