package contains_duplicate

func containsDuplicate(nums []int) bool {

	matches := make(map[int]bool, len(nums))
	for _, n := range nums {
		if matches[n] {
			return true
		}

		matches[n] = true
	}

	return false
}
