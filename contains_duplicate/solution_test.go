package contains_duplicate

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_containsDuplicate(t *testing.T) {
	type args struct {
		nums []int
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "contains duplicate",
			args: args{
				nums: []int{
					1, 2, 3, 1,
				},
			},
			want: true,
		},
		{
			name: "contains duplicate",
			args: args{
				nums: []int{
					1, 2, 3, 4,
				},
			},
			want: false,
		},
		{
			name: "contains duplicate",
			args: args{
				nums: []int{
					1, 1, 1, 3, 3, 4, 3, 2, 4, 2,
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equal(t, tt.want, containsDuplicate(tt.args.nums))
		})
	}
}
